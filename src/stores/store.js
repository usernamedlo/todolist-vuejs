import { reactive } from "vue";

export const storeTask = reactive({
  tasks: [],
  loadTasks() {
    this.tasks = JSON.parse(localStorage.getItem("tasks")) || []; 
  },
  addTask(task) {
    this.tasks.push({ ...task, id: Date.now() });
    this.saveTasksToLocalStorage();
  },
  deleteTask(taskId) {
    this.tasks = this.tasks.filter(task => task.id !== taskId);
    this.saveTasksToLocalStorage();
  },
  updateTask(updatedTask) {
    const index = this.tasks.findIndex(task => task.id === updatedTask.id);
    if (index !== -1) {
      this.tasks[index].title = updatedTask.newTitle;
      this.saveTasksToLocalStorage();
    }
  },
  saveTasksToLocalStorage() {
    localStorage.setItem("tasks", JSON.stringify(this.tasks));
  }
});